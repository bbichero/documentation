Containers
------

- [What is a container](https://jvns.ca/blog/2016/10/10/what-even-is-a-container/)
- [Overview of linux namespaces](`man 7 namespaces`)
- [Run program with namespace of other process](`man nsenter`)
