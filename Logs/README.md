Rsyslog
------

Requirements:
```
Distribution: Debian 10.0
Rsyslog: 8.1
```

Install rsyslog service:
```
apt-get install gnutls-bin rsyslog-gnutls
```

Configuration:
 - minus sign in front of filename `-` it disables buffer flush
