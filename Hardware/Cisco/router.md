Router
------

Set default gateway for paquets
```
ip route 0.0.0.0 0.0.0.0 {GATEWAY_IP}
```

Create ACL:
```
ip access-list standart 10 permit {SOURCE_RANGE} {NETMASK}
```