UsefullCommands
------

Watch deployment in terminal:
```
watch -n0.1 kubectl get pods --all-namespaces
```

List pods:
```
kubectl get pods
```

Get replicats set:
```
kubectl get rs
```

Execute shell command in pods:
```
kubectl exec -it ${POD_NAME} -c shell -- ${SHELL_COMMAND}
```

Forward service port:
```
kubectl port-forward service/${SERVICE_NAME} ${LOCAL_PORT}:${SERVICE_PORT}
```

### Deployment
Get all deployment:
```
kubectl get deployment
```

### Service
Get services list:
```
kubectl get svc
```

### Replica set
Get replicat set list:
```
kubectl get rs
```

### Namespace
Get namespace list:
```
kubectl get ns
```

Create namespace:
```
kubectl create namespace ${NAMESPACE_NAME}
```

Apply yaml config file to a wanted namespace:
```
kubectl apply --namespace=${NAMESPACE_NAME} -f ${YAML_FILE}
```

### Replication controller
Get replication controller list:
```
kunectl get rc
```

### Rollout
Get rollout history:
```
kubectl rollout history deploy/${DEPLOY-NAME}
```

Go back to previous deployment:
```
kubectl rollout undo deploy/${DEPLOY-NAME} --to-revision=1
```

### Label
New label to pod:
```
kubectl label pods ${POD_NAME} ${TAG_NAME}=${TAG_VALUE}
```

List pods and show label:
```
kubectl get pods --show-labels
```

Get pod by environment variables:
```
kubectl get pods -l env=${ENV_VALUE}
```
