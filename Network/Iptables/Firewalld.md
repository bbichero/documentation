Firewalld
------

After each command who update rule, you need to reload:
```
firewall-cmd --reload
```

List zones:
```
firewall-cmd --get-zones
```

List all rules:
```
firewall-cmd --list-all
```

Set interface to zone:
```
sudo firewall-cmd --zone=public --change-interface=ens192
```

Open port
```bash
firewall-cmd --permanent --zone=public --add-port=<port-num>/proto
# example firewall-cmd --permanent --zone=public --add-port=22/tcp
```

Allow range IP on a zone:
```
firewall-cmd --permanent --zone=public --add-source="<ip-range>"
```

Allow all on a zone:
```
firewall-cmd --permanent --zone=<your-zone> --set-target=ACCEPT
```

### Create rich rule for more specific rules
Allow specific ip source and destination port:
```
firewall-cmd --permanent --zone=public --add-rich-rule='rule family=ipv4 source address=<ip-range> port port=<destination-port> protocol=tcp accept'
```

Allow specific ip range source
```
sudo firewall-cmd --zone=public --permanent --add-rich-rule='rule family="ipv4" source address="<ip-range>" accept'
```

With kubernetes you need to change variable in firewalld configuration file:
```
vim /etc/firewalld/firewalld.conf
------
- InvividualCalls=no
+ InvividualCalls=yes
------
sudo systemctl restart firewalld
```
